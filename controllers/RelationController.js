const Relation = require("../models/RelationModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

// Relation Schema
function RelationData(data) {
	this.id = data._id;
	this.parent = data.parent;
	this.person_id = data.person_id
}

/**
 * Relation List.
 * 
 * @returns {Object}
 */
exports.RelationList = [
	function (req, res) {
		try {
			Relation.find({}, "_id person_id parent").then((Relations) => {
				if (Relations.length > 0) {
					return apiResponse.successResponseWithData(res, "Operation success", Relations);
				} else {
					return apiResponse.successResponseWithData(res, "Operation success", []);
				}
			});
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Relation store.
 * 
 * @param {string}      person_id 
 * @param {string}      parent
 * @param {string}      created_by

 * 
 * @returns {Object}
 */
exports.RelationStore = [
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			var Relation1 = new Relation(
				{
					person_id: req.body.person_id,
					parent: req.body.parent,
					created_by: req.body.person_id
				});

			Relation1.save(function (err) {
				if (err) { return apiResponse.ErrorResponse(res, err); }
				let RelationData1 = new RelationData(Relation);
				return apiResponse.successResponseWithData(res, "Relation add Success.", RelationData1);
			});

		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Relation update.
 * 
 * @param {string}      person_id 
 * @param {string}      parent
 * 
 * @returns {Object}
 */
exports.RelationUpdate = [
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			let body = {
				parent: req.body.parent,
				person_id: req.body.person_id,
				_id: req.params.id
			};


			if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
				return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
			} else {
				Relation.findById(req.params.id, function (err, foundRelation) {
					if (foundRelation === null) {
						return apiResponse.notFoundResponse(res, "Relation not exists with this id");
					} else {
						Relation.findByIdAndUpdate(req.params.id, body, {}, function (err) {
							if (err) {
								return apiResponse.ErrorResponse(res, err);
							} else {
								return apiResponse.successResponseWithData(res, "Relation update Success.", body);
							}
						});

					}
				});
			}

		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Relation Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.RelationDelete = [
	function (req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			Relation.findById(req.params.id, function (err, foundRelation) {
				if (foundRelation === null) {
					return apiResponse.notFoundResponse(res, "Relation not exists with this id");
				} else {
					Relation.findByIdAndRemove(req.params.id, function (err) {
						if (err) {
							return apiResponse.ErrorResponse(res, err);
						} else {
							return apiResponse.successResponse(res, "Relation delete Success.");
						}
					});

				}
			});
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Relation Tree.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.RelationTree = [
	function (req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			var params = [
				{
					$match:
					{
						created_by: mongoose.Types.ObjectId(req.params.id)
					}
				},
				{
					$lookup:
					{
						from: "people",
						localField: "person_id",
						foreignField: "_id",
						as: "personData"
					}
				},
				{ $unwind: "$personData" },
			]
			//Relation.aggregate(params, function (err, data) {
			Relation.
				find({ created_by: mongoose.Types.ObjectId(req.params.id) }).
				populate('person_id').
				populate('parent').
				exec(function (err, data) {
					if (err) {
						return apiResponse.ErrorResponse(res, err);
					} else {
						let treeData = data.map(a => new Edge(a.person_id.name + "(" + a.person_id.gender + ")", a.parent.name + "(" + a.parent.gender + ")"));
						const graph = new Graph(treeData);
						let relations = graph.print();
						return apiResponse.successResponseWithData(res, "Relation tree Success.", relations);
					}
				});
		} catch (err) {
			console.log(err)
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


// to generate a graph
function Edge(src, dest) {
	this.src = src;
	this.dest = dest;
}


const Graph = function (edges) {
	let adj = new Map();
	for (let current of edges) {
		const { src, dest } = current;
		if (adj.has(src)) {
			adj.get(src).push(dest);
		} else {
			adj.set(src, [dest]);
		}
	}

	this.add = function (edge) {
		const { src, dest } = edge;
		if (adj.has(src)) {
			adj.get(src).push(dest);
		} else {
			adj.set(src, [dest]);
		}
	}

	this.remove = function (edge) {
		const { src, dest } = edge;
		let srcList = adj.get(src);
		srcList = srcList.filter(e => e !== dest);

		if (srcList.length === 0) {
			adj.delete(src);
		} else {
			adj.set(src, srcList);
		}

		return true;
	}

	this.print = function () {
		let n = adj.size;
		let res = []
		for (let src of adj.keys()) {
			let str = "";
			for (let dest of adj.get(src)) {
				str += "(" + src + " ——> " + dest + ")";
			}
			console.log(str);
			res.push(str)
		}
		return res
	}

	//Return graph
	this.getList = () => adj;
}