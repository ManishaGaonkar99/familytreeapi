const Person = require("../models/PersonModel");
const Relations = require("../models/RelationModel");

const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
var mongoose = require("mongoose");
const { set, ObjectId } = require("mongoose");
mongoose.set("useFindAndModify", false);

// Person Schema
function PersonData(data) {
    this.id = data._id;
    this.name = data.name;
    this.gender = data.gender;
}

/**
 * Person List.
 * 
 * @returns {Object}
 */
exports.PersonList = [
    function (req, res) {
        try {
            Person.find({}, "_id name gender").then((Persons) => {
                if (Persons.length > 0) {
                    return apiResponse.successResponseWithData(res, "Operation success", Persons);
                } else {
                    return apiResponse.successResponseWithData(res, "Operation success", []);
                }
            });
        } catch (err) {
            //throw error in json response with status 500. 
            return apiResponse.ErrorResponse(res, err);
        }
    }
];


/**
 * Person Detail.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.PersonDetail = [
    function (req, res) {
        if (!req.query.relation) {
            return apiResponse.ErrorResponse(res, "Please pass a realtion filter", {});
        }
        let gender = "M";
        switch (req.query.relation.toLowerCase()) {
            case "mother":
            case "grandmother":
            case "sister":
            case "aunt": {
                gender = "F"
                break;
            }
        }
        try {
            var params = []
            if (req.query.relation.toLowerCase() == "mother" || req.query.relation.toLowerCase() == "father") {
                params = [
                    {
                        $match:
                            { person_id: mongoose.Types.ObjectId(req.params.id) }
                    },
                    {
                        $lookup:
                        {
                            from: "people",
                            localField: "parent",
                            foreignField: "_id",
                            as: "personData"
                        }
                    },
                    { $unwind: "$personData" },
                    { $match: { "personData.gender": gender } }
                ]

                Relations.aggregate(params, function (err, foundPerson) {
                    if (err) {
                        return apiResponse.ErrorResponse(res, err);
                    } else {
                        return apiResponse.successResponseWithData(res, "Person retrieve Success.", foundPerson);
                    }
                });

            } else if (req.query.relation.toLowerCase() == "brother" || req.query.relation.toLowerCase() == "sister") {
                Relations.find({ person_id: req.params.id }, function (err, foundPerson) {
                    if (foundPerson.length === 0) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        params = [
                            {
                                $match:
                                    { parent: mongoose.Types.ObjectId(foundPerson[0].parent) }
                            },
                            {
                                $lookup:
                                {
                                    from: "people",
                                    localField: "person_id",
                                    foreignField: "_id",
                                    let: { person_id: "$person_id" },
                                    pipeline: [{
                                        $match: {
                                            $expr: { $ne: ["$$person_id", mongoose.Types.ObjectId(req.params.id)] }
                                        }
                                    }],
                                    as: "personData"
                                }
                            },
                            { $unwind: "$personData" },
                            { $match: { "personData.gender": gender } }
                        ]
                        Relations.aggregate(params, function (err, data) {
                            if (err) {
                                return apiResponse.ErrorResponse(res, err);
                            } else {
                                return apiResponse.successResponseWithData(res, "Person retrieve Success.", data);
                            }
                        });
                    }
                });

            } else if (req.query.relation.toLowerCase() == "grandmother" || req.query.relation.toLowerCase() == "grandfather") {
                Relations.find({ person_id: req.params.id }, function (err, foundPerson) {
                    if (foundPerson.length === 0) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        let myParents = foundPerson.map(a => mongoose.Types.ObjectId(a.parent));
                        Relations.find({ person_id: { $in: myParents } }, function (err, foundPerson1) {
                            if (foundPerson1.length === 0) {
                                return apiResponse.notFoundResponse(res, "Person not exists with this id");
                            } else {
                                let parents = foundPerson1.map(a => mongoose.Types.ObjectId(a.parent));
                                params = [
                                    {
                                        $match:
                                        {
                                            parent: { $in: parents },
                                            person_id: { $in: myParents }
                                        }
                                    },
                                    {
                                        $lookup:
                                        {
                                            from: "people",
                                            localField: "parent",
                                            foreignField: "_id",
                                            as: "personData"
                                        }
                                    },
                                    { $unwind: "$personData" },
                                    { $match: { "personData.gender": gender } }
                                ]
                                Relations.aggregate(params, function (err, data) {
                                    if (err) {
                                        return apiResponse.ErrorResponse(res, err);
                                    } else {
                                        return apiResponse.successResponseWithData(res, "Person retrieve Success.", data);
                                    }
                                });
                            }
                        })
                    }
                });
            } else if (req.query.relation.toLowerCase() == "uncle" || req.query.relation.toLowerCase() == "aunt") {
                Relations.find({ person_id: req.params.id }, function (err, foundPerson) {
                    if (foundPerson.length === 0) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        let myParents = foundPerson.map(a => mongoose.Types.ObjectId(a.parent));
                        Relations.find({ person_id: { $in: myParents } }, function (err, foundPerson1) {
                            if (foundPerson1.length === 0) {
                                return apiResponse.notFoundResponse(res, "Person not exists with this id");
                            } else {
                                let parents = foundPerson1.map(a => mongoose.Types.ObjectId(a.parent));
                                params = [
                                    {
                                        $match:
                                        {
                                            parent: { $in: parents },
                                            person_id: { $not: { $in: myParents } }
                                        }
                                    },
                                    {
                                        $lookup:
                                        {
                                            from: "people",
                                            localField: "person_id",
                                            foreignField: "_id",
                                            as: "personData"
                                        }
                                    },
                                    { $unwind: "$personData" },
                                    { $match: { "personData.gender": gender } }
                                ]
                                Relations.aggregate(params, function (err, data) {
                                    if (err) {
                                        return apiResponse.ErrorResponse(res, err);
                                    } else {
                                        let finalData = data.filter((v, i, a) => a.findIndex(t => (t.name === v.name)) === i)

                                        return apiResponse.successResponseWithData(res, "Person retrieve Success.", finalData);
                                    }
                                });
                            }
                        })
                    }
                });
            } else if (req.query.relation.toLowerCase() == "cousins") {
                Relations.find({ person_id: req.params.id }, function (err, foundPerson) {
                    if (foundPerson.length === 0) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        let myParents = foundPerson.map(a => mongoose.Types.ObjectId(a.parent));
                        Relations.find({ person_id: { $in: myParents } }, function (err, foundPerson1) {
                            if (foundPerson1.length === 0) {
                                return apiResponse.notFoundResponse(res, "Person not exists with this id");
                            } else {
                                let parents = foundPerson1.map(a => mongoose.Types.ObjectId(a.parent));// soiru , usha
                                params = [
                                    {
                                        $match:
                                        {
                                            parent: { $in: parents },
                                            person_id: { $not: { $in: myParents } }
                                        }
                                    }
                                ]
                                Relations.aggregate(params, function (err, data) {
                                    if (err) {
                                        return apiResponse.ErrorResponse(res, err);
                                    } else {
                                        let cousinsParents = data.filter((v, i, a) => a.findIndex(t => (t.name === v.name)) === i).map(a => mongoose.Types.ObjectId(a.person_id));
                                        Relations.aggregate([
                                            {
                                                $match:
                                                    { parent: { $in: cousinsParents } }
                                            },
                                            {
                                                $lookup:
                                                {
                                                    from: "people",
                                                    localField: "person_id",
                                                    foreignField: "_id",
                                                    as: "personData"
                                                }
                                            },
                                            { $unwind: "$personData" },
                                            //{ $match:{"personData.gender":gender}}
                                        ], function (err, foundPerson) {
                                            if (foundPerson.length === 0) {
                                                return apiResponse.notFoundResponse(res, "Person not exists with this id");
                                            } else {
                                                return apiResponse.successResponseWithData(res, "Person retrieve Success.", foundPerson);
                                            }
                                        });
                                    }
                                });
                            }
                        })
                    }
                });
            } else if (req.query.relation.toLowerCase() == "children") {
                var params = [
                    {
                        $match:
                            { parent: mongoose.Types.ObjectId(req.params.id) }
                    },
                    {
                        $lookup:
                        {
                            from: "people",
                            localField: "person_id",
                            foreignField: "_id",
                            as: "personData"
                        }
                    },
                    { $unwind: "$personData" }
                ]
                Relations.aggregate(params, function (err, foundPerson) {
                    if (err) {
                        return apiResponse.ErrorResponse(res, err);
                    } else {
                        return apiResponse.successResponseWithData(res, "Person retrieve Success.", foundPerson);
                    }
                });
            } else if (req.query.relation.toLowerCase() == "grandchildren") {
                Relations.find({ parent: req.params.id }, function (err, foundPerson) {
                    if (foundPerson.length === 0) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        let myChildren = foundPerson.map(a => mongoose.Types.ObjectId(a.person_id)); // neil
                        var params = [
                            {
                                $match:
                                    { parent: { $in: myChildren } }
                            },
                            {
                                $lookup:
                                {
                                    from: "people",
                                    localField: "person_id",
                                    foreignField: "_id",
                                    as: "personData"
                                }
                            },
                            { $unwind: "$personData" }
                        ]
                        Relations.aggregate(params, function (err, foundPerson) {
                            if (err) {
                                return apiResponse.ErrorResponse(res, err);
                            } else {
                                return apiResponse.successResponseWithData(res, "Person retrieve Success.", foundPerson);
                            }
                        });
                    }

                });
            } else {
                return apiResponse.successResponseWithData(res, "No Data.", []);
            }


        } catch (err) {
            //throw error in json response with status 500. 
            return apiResponse.ErrorResponse(res, err);
        }
    }
];

/**
 * Person store.
 * 
 * @param {string}      name 
 * @param {string}      gender
 * 
 * @returns {Object}
 */
exports.PersonStore = [

    sanitizeBody("*").escape(),
    (req, res) => {
        try {
            var Person1 = new Person(
                {
                    name: req.body.name,
                    gender: req.body.gender
                });
            Person1.save(function (err) {
                if (err) { return apiResponse.ErrorResponse(res, err); }
                let PersonData = new Person(Person1);
                return apiResponse.successResponseWithData(res, "Person add Success.", PersonData);
            });
        } catch (err) {
            return apiResponse.ErrorResponse(res, err);
        }
    }
];

/**
 * Person update.
 * 
 * @param {string}      name 
 * @param {string}      gender 
 * @returns {Object}
 */
exports.PersonUpdate = [

    sanitizeBody("*").escape(),
    (req, res) => {
        try {

            let body = {
                name: req.body.name,
                gender: req.body.gender,
                _id: req.params.id
            }


            if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
                return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
            } else {
                Person.findById(req.params.id, function (err, foundPerson) {
                    if (foundPerson === null) {
                        return apiResponse.notFoundResponse(res, "Person not exists with this id");
                    } else {
                        Person.findByIdAndUpdate(req.params.id, body, {}, function (err) {
                            if (err) {
                                return apiResponse.ErrorResponse(res, err);
                            } else {
                                return apiResponse.successResponseWithData(res, "Person update Success.", body);
                            }
                        });

                    }
                });
            }

        } catch (err) {
            return apiResponse.ErrorResponse(res, err);
        }
    }
];

/**
 * Person Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.PersonDelete = [

    function (req, res) {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
        }
        try {
            Person.findById(req.params.id, function (err, foundPerson) {
                if (foundPerson === null) {
                    return apiResponse.notFoundResponse(res, "Person not exists with this id");
                } else {
                    Person.findByIdAndRemove(req.params.id, function (err) {
                        if (err) {
                            return apiResponse.ErrorResponse(res, err);
                        } else {
                            return apiResponse.successResponse(res, "Person delete Success.");
                        }
                    });

                }
            });
        } catch (err) {
            //throw error in json response with status 500. 
            return apiResponse.ErrorResponse(res, err);
        }
    }
];