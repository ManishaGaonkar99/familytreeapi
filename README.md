# Family Tree

[API Documentation] (https://web.postman.co/documentation/4216673-6d9ccf88-91af-48d8-94e3-7fe7130f965e/publish?workspaceId=588a846f-82f3-4fbe-8415-8efa82e4f639)

[Refer to Sample Data] (SampleData.json)

## Build
1. Enable to create and manage a family tree.
2. Build extensive REST APIs to execute CRUD operations on different entities.
3. User can create a family and it's members, establish relationship between members.
4. User can see whole family tree.
5. User can see a member details with immediate relatives.
6. User can search for following relatives of a member -
a. Mother
b. Father
c. Brother
d. Sister
e. Grandfather
f. Grandmother
g. Grandchildren
h. Uncle
i. Aunt
j. Cousins


## Scope of Improvements
- Can be written without ORM
- Can include more unit test cases