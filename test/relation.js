const { chai, server, should } = require("./testConfig");
const RelationModel = require("../models/RelationModel");

/**
 * Test cases to test all the relation APIs
 * Covered Routes:
 * (1) Store relation
 * (2) Get all relations
 * (3) Update relation
 * (4) Delete relation
 */

describe("Relation", () => {
	//Before each test we empty the database
	before((done) => { 
		RelationModel.deleteMany({}, (err) => { 
			done();           
		});        
	});

	// Prepare data for testing
	const testData = {
		"person_id":"61c035b2cb80b99955d2bac1",
		"parent":"61c035b2cb80b99955d2bac2"	
	};


	/*
  * Test the /POST route
  */
	describe("/POST Relation Store", () => {
		it("It should store relation", (done) => {
			chai.request(server)
				.post("/api/relation")
				.send(testData)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Relation add Success.");
					done();
				});
		});
	});

	/*
  * Test the /GET route
  */
	describe("/GET All relations", () => {
		it("it should GET all the relations", (done) => {
			chai.request(server)
				.get("/api/relation")
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Operation success");
					testData._id = res.body.data[0]._id;
					done();
				});
		});
	});

	/*
  * Test the /PUT/:id route
  */
	describe("/PUT/:id relation", () => {
		it("it should PUT the relations", (done) => {
			chai.request(server)
				.put("/api/relation/"+testData._id)
				.send(testData)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Relation update Success.");
					done();
				});
		});
	});

	/*
  * Test the /DELETE/:id route
  */
	describe("/DELETE/:id relation", () => {
		it("it should DELETE the relations", (done) => {
			chai.request(server)
				.delete("/api/relation/"+testData._id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Relation delete Success.");
					done();
				});
		});
	});
});