const { chai, server, should } = require("./testConfig");
const PersonModel = require("../models/PersonModel");

/**
 * Test cases to test all the person APIs
 * Covered Routes:
 * (1) Store person
 * (2) Get all people
 * (3) Update person
 * (4) Delete person
 */

describe("Person", () => {
	//Before each test we empty the database
	before((done) => { 
		PersonModel.deleteMany({}, (err) => { 
			done();           
		});        
	});

	// Prepare data for testing
	const testData = {
		"name":"Test User",
		"gender":"F"	
	};


	/*
  * Test the /POST route
  */
	describe("/POST Person Store", () => {
		it("It should store person", (done) => {
			chai.request(server)
				.post("/api/person")
				.send(testData)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Person add Success.");
					done();
				});
		});
	});

	/*
  * Test the /GET route
  */
	describe("/GET All people", () => {
		it("it should GET all the people", (done) => {
			chai.request(server)
				.get("/api/person")
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Operation success");
					testData._id = res.body.data[0]._id;
					done();
				});
		});
	});

	/*
  * Test the /PUT/:id route
  */
	describe("/PUT/:id person", () => {
		it("it should PUT the people", (done) => {
			chai.request(server)
				.put("/api/person/"+testData._id)
				.send(testData)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Person update Success.");
					done();
				});
		});
	});

	/*
  * Test the /DELETE/:id route
  */
	describe("/DELETE/:id person", () => {
		it("it should DELETE the people", (done) => {
			chai.request(server)
				.delete("/api/person/"+testData._id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Person delete Success.");
					done();
				});
		});
	});
});