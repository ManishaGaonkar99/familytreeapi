var express = require("express");
const RelationController = require("../controllers/RelationController");

var router = express.Router();

router.post("/", RelationController.RelationStore);
router.get("/", RelationController.RelationList);
router.get("/:id/tree", RelationController.RelationTree);
router.put("/:id", RelationController.RelationUpdate);
router.delete("/:id", RelationController.RelationDelete);

module.exports = router;