var express = require("express");
var personRouter = require("./person");
var relationRouter = require("./relation");

var app = express();

app.use("/person/", personRouter);
app.use("/relation/", relationRouter);

module.exports = app;