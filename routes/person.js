var express = require("express");
const PersonController = require("../controllers/PersonController");

var router = express.Router();

router.post("/", PersonController.PersonStore);
router.get("/", PersonController.PersonList);
router.get("/:id/search", PersonController.PersonDetail);
router.put("/:id", PersonController.PersonUpdate);
router.delete("/:id", PersonController.PersonDelete);

module.exports = router;