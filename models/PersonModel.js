var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var PersonSchema = new Schema({
	name: {type: String, required: true},
    gender: {type: String, required: true}
}, {timestamps: true});

module.exports = mongoose.model("Person", PersonSchema, "people");