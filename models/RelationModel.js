var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var RelationSchema = new Schema({
	person_id: { type: Schema.ObjectId, ref: "Person", required: true },
    parent: { type: Schema.ObjectId, ref: "Person", required: true },
    created_by: { type: Schema.ObjectId, ref: "Person", required: true }
}, {timestamps: true});

module.exports = mongoose.model("Relations", RelationSchema,"relationship");